import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

class TestPrimerEntregable(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    #@unittest.skip("temp")
    def test_show_message(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
        text_input = driver.find_element_by_xpath('//input[@id="user-message"]')
        show_button = driver.find_element_by_xpath("//button[contains(text(),'Show Message')]")
        text_outut = driver.find_element_by_xpath('//div[@id="user-message"]/span')

        text_input.send_keys('milton')
        show_button.click()
        time.sleep(5)
        self.assertEqual('milton', text_outut.text)

    #@unittest.skip("temp")
    def test_check_message(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html")

        time.sleep(2)
        check_button = driver.find_element_by_xpath("//label[text()='Click on this check box']",)
        check_button.click()

        time.sleep(5)
        show_message = driver.find_element_by_id('txtAge')
        show_message = show_message.text
        self.assertEqual(show_message, "Success - Check box is checked")

    #@unittest.skip("temp")
    def test_check_radio_button(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html")

        radio_button_male = driver.find_element_by_xpath(
            "/html/body/div[2]/div/div[2]/div[1]/div[2]/label[1]/input"
        )
        get_values_button = driver.find_element_by_xpath(
            "//button[contains(text(),'Get Checked value')]"
        )
        text_output = driver.find_element_by_xpath(
            "//p[@class='radiobutton']"
        )

        radio_button_male.click()
        get_values_button.click()
        self.assertRegex(text_output.text, "Male")

if __name__ == '__main__':
    unittest.main()